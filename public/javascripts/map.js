var mymap = L.map('mainmap').setView([4.60934, -74.17707], 19);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);
// var marker= L.marker([4.609373, -74.176693]).addTo(mymap);
// var marker= L.marker([4.609302, -74.176123]).addTo(mymap);

$.ajax({
   dataType: "json",
   url: "api/bicicletas",
   success: function(result) {
       console.log(result);
       result.bicicletas.forEach(function(bici){
        L.marker(bici.ubicacion,{title: 'ID: ' + bici.id + '\nModelo: ' + bici.modelo + '\nColor: ' + bici.color}).addTo(mymap);
       });
   }
});