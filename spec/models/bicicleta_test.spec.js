var Bicicleta = require('../../models/bicicleta');
beforeEach(() => {Bicicleta.allBicis=[];});
describe ('Bicicleta.allBicis',() => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add',() => {
    it('agregamos una',() => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1,'rojo','urbana',[4.609373, -74.176693]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById',() => {
    it('debe devolver la bici co id 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBicis = new Bicicleta(1,"verde","urbana");
        var aBicis2 = new Bicicleta(2,"verde","urbana");
        Bicicleta.add(aBicis);
        Bicicleta.add(aBicis2);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBicis.color);
        expect(targetBici.modelo).toBe(aBicis.modelo);
    });
});

describe('Bicicleta.removeById',() => {
    it('debe devolver la bici co id 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBicis = new Bicicleta(1,"verde","urbana");
        var aBicis2 = new Bicicleta(2,"verde","urbana");
        Bicicleta.add(aBicis);
        Bicicleta.add(aBicis2);
        var targetBici = Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
    });
});