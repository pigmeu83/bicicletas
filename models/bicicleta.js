/*modelo bicicleta puente entre vista y el controlador*/ 
var Bicicleta = function(id,color,modelo,ubicacion){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion
}
/* regresa en string los datos*/
Bicicleta.prototype.toString=function(){
    return 'id: ' + this.id + " | color: "+this.color;
}
/*array para las bicis sin bd */
Bicicleta.allBicis=[];

/* agregar datos al array*/
Bicicleta.add =function(aBici){
    Bicicleta.allBicis.push(aBici);
};

// var a = new Bicicleta(1,'rojo','urbana',[4.609373, -74.176693]);
// var b = new Bicicleta(2,'blanco','urbana',[4.609302, -74.176123]);
// var c= new Bicicleta(3,'azul','montaña',[4.609162, -74.176366]);
// Bicicleta.add(a);
// Bicicleta.add(b);
// Bicicleta.add(c);
Bicicleta.findById=function(aBiciId){
   
    var aBici = Bicicleta.allBicis.find(x=>x.id==aBiciId);
    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBici}`);
}

Bicicleta.removeById=function(aBiciId){
    var aBici = Bicicleta.findById(aBiciId);
    for(var i=0;i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

module.exports = Bicicleta;

